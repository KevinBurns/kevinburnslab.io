module.exports = {
  siteMetadata: {
    title: 'AixWebMedia GmbH - web media devops agile digital project consultants',
    author: 'Kevin Burns',
    description: 'Agile & Digital consultancy delivering change in development thinking to launch successful products in Media, Telecom, Retail & Software markets',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'gatsby-starter-default',
        short_name: 'starter',
        start_url: '/',
        background_color: '#180c23',
        theme_color: '#180c23',
        display: 'minimal-ui',
        icon: 'src/images/icon.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
  ],
}
